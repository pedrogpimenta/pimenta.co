var gulp = require('gulp'),
    sass = require('gulp-sass'),
    combineMq = require('gulp-combine-mq'),
    autoprefixer = require('gulp-autoprefixer'),
    csso = require('gulp-csso'),
    watch = require('gulp-watch'),
    livereload = require('gulp-livereload');

gulp.task('sass', function() {
  gulp.src('./files/main.sass')
    .pipe(sass().on('error', sass.logError)) // SASS to CSS
    .pipe(combineMq({ // Combine media queries
        beautify: false // no beautify
    }))
    .pipe(autoprefixer({ // Add browser vendor prefixes
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(csso())
    .pipe(gulp.dest('./../css'))
    .pipe(livereload()); // Live reload
});

gulp.task('default', ['sass'], function () {
  livereload.listen(); // Live reload
  gulp.watch('./**/*.sass', ['sass']); // Watch SASS
});
