<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://antimatter/antimatter.yaml',
    'modified' => 1470285686,
    'data' => [
        'enabled' => true,
        'dropdown' => [
            'enabled' => false
        ]
    ]
];
